# InkPageIndicator
 
#### 项目介绍
- 项目名称：InkPageIndicator
- 所属系列：openharmony的第三方组件适配移植
- 功能：一个页面指示器
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk5，DevEco Studio2.1 beta3 
- 基线版本：Release 1.3.0

#### 效果演示

![效果演示](./printscreen/InkPageIndicator.gif "截图1") 

#### 安装教程

1.在项目根目录下的build.gradle文件中，
 ```
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/snapshots/'
        }
    }
}
 ```
2.在entry模块的build.gradle文件中，
 ```
 dependencies {
    implementation('com.gitee.chinasoft_ohos:InkPageIndicator:0.0.1-SNAPSHOT')
    ......  
 }
 ```


在sdk5，DevEco Studio2.1 beta3下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

将自定义view添加到XML中，使用方法如下:
```
<com.pixelcan.inkpageindicator.InkPageIndicator
        ohos:id="$+id:indicator"
        ohos:height="20vp"
        ohos:width="match_content"
        ohos:center_in_parent="true"
        ohos:align_parent_bottom="true"
        ohos:bottom_margin="100vp"
        ohos:horizontal_center="true"
        app:ipi_animationDuration="420"
        app:ipi_dotDiameter="40"
        app:ipi_dotGap="28"
        />
```

完整调用:

```
        inkPageIndicator = (InkPageIndicator) findComponentById(ResourceTable.Id_indicator);
        inkPageIndicator.setViewPager(pageSlider);

```





| Method                      | Description                      |
| :-------------------------- | :------------------------------- |
| setViewPager                | 设置对应PageSlider                    | 

 


#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

火绒安全病毒安全检测通过

当前版本demo功能与原组件基本无差异 

#### 版本迭代

- 0.0.1_SNAPSHOT

#### 版权和许可信息

```
Copyright 2018 David Păcioianu

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
