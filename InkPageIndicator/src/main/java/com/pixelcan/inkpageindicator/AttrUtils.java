package com.pixelcan.inkpageindicator;

import ohos.agp.components.AttrSet;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import java.io.IOException;
import java.io.InputStream;

/**
 *
 *  自定义属性工具类
 *
 *  注意： 宽高都为 match_content 且无实际内容时构造方法不会调用
 *  使用方法：
 *  xxx extends Component
 *  获取自定义属性：
 *  String count = AttrUtils.getStringFromAttr(attrSet,"cus_count","0");
 *
 *  属性定义：
 *  布局头中加入  xmlns:hap="http://schemas.huawei.com/apk/res/ohos" 使用hap区分自定义属性与系统属性
 *  即可使用 hap:cus_count="2"  不加直接使用ohos:cus_count="2"
 *
 *  与xxx区别：xxx在xml预先定义好属性，可通过app: 点选，ohos暂未发现对应方法
 *
 */
public class AttrUtils {
    /**
     *
     * @param attrSet
     * @param name
     * @param defaultValue
     * @return String
     */
    public static String getString(AttrSet attrSet, String name, String defaultValue){
        String value = defaultValue;
        try {
            if(attrSet.getAttr(name)!= null && attrSet.getAttr(name).isPresent()){
                value = attrSet.getAttr(name).get().getStringValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     *
     * @param context
     * @param attrSet
     * @param name
     * @param defaultValue
     * @return Integer
     */
    public static Integer getInteger(Context context, AttrSet attrSet, String name, Integer defaultValue){
        Integer value = defaultValue;
        try {
            if(attrSet.getAttr(name)!= null && attrSet.getAttr(name).isPresent()){
                value = attrSet.getAttr(name).get().getIntegerValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     *
     * @param attrSet
     * @param name
     * @param defaultValue
     * @return float
     */
    public static float getFloat(AttrSet attrSet,String name,float defaultValue){
        float value = defaultValue;
        try {
            if(attrSet.getAttr(name)!= null && attrSet.getAttr(name).isPresent()){
                value = attrSet.getAttr(name).get().getFloatValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     *
     * @param attrSet
     * @param name
     * @param defaultValue
     * @return  boolean
     */
    public static boolean getBoolean(AttrSet attrSet,String name,boolean defaultValue){
        boolean value = defaultValue;
        try {
            if(attrSet.getAttr(name)!= null && attrSet.getAttr(name).isPresent()){
                value = attrSet.getAttr(name).get().getBoolValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     *
     * @param attrSet
     * @param name
     * @param defaultValue
     * @return Long
     */
    public static Long getLong(AttrSet attrSet,String name,Long defaultValue){
        Long value = defaultValue;
        try {
            if(attrSet.getAttr(name)!= null && attrSet.getAttr(name).isPresent()){
                value = attrSet.getAttr(name).get().getLongValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     *
     * @param attrSet
     * @param name
     * @param defaultValue
     * @return 色值
     */
    public static int getColor(AttrSet attrSet,String name,int defaultValue){
        int value = defaultValue;
        try {
            if(attrSet.getAttr(name)!= null && attrSet.getAttr(name).isPresent()){
                value = attrSet.getAttr(name).get().getColorValue().getValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     *
     * @param context
     * @param attrSet
     * @param name
     * @param defaultValue
     * @return  Float
     */
    public static Float getDimensionPixelSizeFloat(Context context,AttrSet attrSet, String name,float defaultValue){
        float fp = defaultValue;
        try {
            if(attrSet.getAttr(name)!= null && attrSet.getAttr(name).isPresent()){
                fp = attrSet.getAttr(name).get().getFloatValue();
            }
            Display display = DisplayManager.getInstance().getDefaultDisplay(context).get();
            float sca = display.getAttributes().scalDensity;
            return fp * sca + 0.5f * (fp >= 0 ? 1 : -1);
        } catch (Exception e) {
            e.printStackTrace();
            return fp;
        }

    }

    /**
     *
     * @param context
     * @param attrSet
     * @param name
     * @param defaultValue
     * @return int
     */
    public static int getDimensionPixelSizeInt(Context context,AttrSet attrSet, String name,int defaultValue){
        int fp = defaultValue;
        try {
            if(attrSet.getAttr(name)!= null && attrSet.getAttr(name).isPresent()){
                fp = attrSet.getAttr(name).get().getIntegerValue();
            }
            Display display = DisplayManager.getInstance().getDefaultDisplay(context).get();
            float sca = display.getAttributes().scalDensity;
            float result = fp * sca + 0.5f * (fp >= 0 ? 1 : -1);
            return Float.floatToIntBits(result);
        } catch (Exception e) {
            e.printStackTrace();
            return fp;
        }

    }

    /**
     *
     * @param attrSet
     * @param name
     * @param context
     * @param defaultValue
     * @return PixelMap
     */
    public static PixelMap getPixelMap(AttrSet attrSet, String name, Context context,PixelMap defaultValue){
        PixelMap value = defaultValue;
            InputStream inputStream = null;
            try {
                // 创建图像数据源ImageSource对象
                inputStream = context.getResourceManager().getResource(attrSet.getAttr(name).get().getIntegerValue());
                ImageSource.SourceOptions srcOpts = new ImageSource.SourceOptions();
                srcOpts.formatHint = "image/jpg";
                ImageSource imageSource = ImageSource.create(inputStream, srcOpts);

                // 设置图片参数
                ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
                value = imageSource.createPixelmap(decodingOptions);
                return value;
            } catch (IOException e) {
//            HiLog.info(LABEL_LOG, "IOException");
            } catch (NotExistException e) {
//            HiLog.info(LABEL_LOG, "NotExistException");
            } finally {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
//                    HiLog.info(LABEL_LOG, "inputStream IOException");
                    }
                }
            }
            return value;
        }



}
