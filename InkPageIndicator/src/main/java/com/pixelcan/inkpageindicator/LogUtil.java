package com.pixelcan.inkpageindicator;

 import java.util.logging.Level;
import java.util.logging.Logger;

 public class LogUtil {
     private static Logger logger;
     private static boolean isPrintStatus = false;

     private static Logger createLogger(String tag) {
         if (logger == null) {
             logger = Logger.getLogger(tag);
         }
         return logger;
     }

     public static void setPrintStatus(boolean isPrint){
         isPrintStatus = isPrint;
     }

     public static void loge(String msg) {
         if(isPrintStatus){
             createLogger("MY_TAG").log(Level.SEVERE, msg);
         }
     }

     public static void loge(String tag, String msg) {
         if(isPrintStatus){
             createLogger(tag).log(Level.SEVERE, msg);
         }
     }



 }
