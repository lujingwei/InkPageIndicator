package com.pixelcan.ipidemo.slice;

import com.pixelcan.inkpageindicator.InkPageIndicator;
import com.pixelcan.ipidemo.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbPalette;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Rect;
import ohos.media.image.ImageSource;
import ohos.media.photokit.metadata.AVStorage;
import ohos.utils.net.Uri;

import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.util.ArrayList;

/**
 *
 */
public class PageSliderSlice extends AbilitySlice{

    private int mCurrentPosition = 0;
    private PageSlider pageSlider;
    private PageSliderIndicator pageSliderIndicator;
    private InkPageIndicator inkPageIndicator;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_pageslider_demo);
        pageSlider = (PageSlider) findComponentById(ResourceTable.Id_pageslider);
        pageSliderIndicator = (PageSliderIndicator) findComponentById(ResourceTable.Id_pagesliderindicator);

        inkPageIndicator = (InkPageIndicator) findComponentById(ResourceTable.Id_indicator);

        initPageSlider();
        initPageSliderIndicator();


    }

    private void initPageSliderIndicator() {
        pageSliderIndicator.setViewPager(pageSlider);

        ShapeElement normalElement = new ShapeElement();
        normalElement.setShape(ShapeElement.OVAL);
        normalElement.setBounds(new Rect(0,0,20,20));
        normalElement.setShaderType(ShapeElement.LINEAR_GRADIENT_SHADER_TYPE);
        normalElement.setRgbColor(RgbPalette.BLUE);

        ShapeElement selectElement = new ShapeElement();
        selectElement.setShape(ShapeElement.OVAL);
        selectElement.setBounds(new Rect(0,0,20,20));
        selectElement.setShaderType(ShapeElement.LINEAR_GRADIENT_SHADER_TYPE);
        selectElement.setRgbColor(RgbPalette.RED);

        pageSliderIndicator.setItemElement(normalElement,selectElement);
        pageSliderIndicator.setItemOffset(30);

        inkPageIndicator.setViewPager(pageSlider);

    }

    private void initPageSlider() {
        LayoutScatter layoutScatter = LayoutScatter.getInstance(getContext());
        DependentLayout layoutOne = (DependentLayout) layoutScatter.parse(ResourceTable.Layout_page_one, null, false);
        DependentLayout layoutTwo = (DependentLayout) layoutScatter.parse(ResourceTable.Layout_page_two, null, false);
        DependentLayout layoutThree = (DependentLayout) layoutScatter.parse(ResourceTable.Layout_page_three, null, false);
        DependentLayout layout2 = (DependentLayout) layoutScatter.parse(ResourceTable.Layout_page_two, null, false);
        DependentLayout layout3 = (DependentLayout) layoutScatter.parse(ResourceTable.Layout_page_three, null, false);


        ArrayList<Component> pageView = new ArrayList();
//        pageView.add(layoutOne);
        pageView.add(layoutTwo);
        pageView.add(layoutThree);
        pageView.add(layout2);
        pageView.add(layout3);

        pageSlider.setProvider(new PageSliderProvider() {
            @Override
            public int getCount() {
                return pageView.size();
            }

            @Override
            public Object createPageInContainer(ComponentContainer componentContainer, int i) {
                componentContainer.addComponent(pageView.get(i));
                return pageView.get(i);
            }

            @Override
            public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
                componentContainer.removeComponent(pageView.get(i));
            }

            @Override
            public boolean isPageMatchToObject(Component component, Object o) {
                return component == o;
            }
        });
        pageSlider.setCurrentPage(mCurrentPosition);

    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

}
