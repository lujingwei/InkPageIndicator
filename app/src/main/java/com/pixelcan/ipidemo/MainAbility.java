package com.pixelcan.ipidemo;

import com.pixelcan.ipidemo.slice.MainAbilitySlice;

import com.pixelcan.ipidemo.slice.PageSliderSlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(PageSliderSlice.class.getName());
    }
}